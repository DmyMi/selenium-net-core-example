# Selenium testing with .Net (core)
1. Create solution
```
dotnet new sln
```

2. Create directories for main project and tests
```
mkdir Pages
mkdir AutoTests
```

3. Create `pages` project and add it to solution
```
cd Pages
dotnet new classlib
cd ..
dotnet sln add Pages/Pages.csproj
```

4. Create Tests Project and add reference to pages project
```
cd AutoTests
dotnet new nunit
dotnet add reference ../Pages/Pages.csproj
cd ..
dotnet sln add AutoTests/AutoTests.csproj
```

5. Add Selenium Packages in Pages project
```
dotnet add package Selenium.WebDriver
dotnet add package Selenium.Support
dotnet add package Selenium.Chrome.WebDriver
dotnet add package DotNetSeleniumExtras.PageObjects
```