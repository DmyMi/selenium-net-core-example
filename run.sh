#!/bin/bash

docker network create grid
docker run -d -p 4444:4444 --net grid -v /dev/shm:/dev/shm --name selenium-hub selenium/hub
docker run -d --net grid -e HUB_HOST=selenium-hub -e SCREEN_WIDTH=1280 -e SCREEN_HEIGHT=768 -v /dev/shm:/dev/shm selenium/node-chrome
docker run -d -p 5900:5900 --net grid -e HUB_HOST=selenium-hub -e SCREEN_WIDTH=1280 -e SCREEN_HEIGHT=768 -e SCREEN_DEPTH=24 -v /dev/shm:/dev/shm selenium/node-firefox-debug