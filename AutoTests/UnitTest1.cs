using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using Pages;

namespace Tests
{
    [TestFixture]
    public class Tests
    {

        [Test]
        public void Test1()
        {
            using (IWebDriver driver = new ChromeDriver("/home/dima/DotNet/selenium/AutoTests"))
            {
                var google = new Google(driver);
                google.NavigateToGoogle();
                google.SearchText("selenium");
                ResultPage result = google.Click();
                Assert.True(result.isResultPresent());
            }
        }

    [Test]
    public void SuperTest()
    {
        using (var driver = new ChromeDriver("/home/dima/DotNet/selenium/AutoTests"))
        {
        driver.Navigate().GoToUrl(@"https://google.com/");
        IWebElement search = driver.FindElement(By.Name("q"));
        search.Clear();
        search.SendKeys("selenium");
        IWebElement button = driver.FindElement(By.Name("btnK"));
        IWebElement img = driver.FindElement(By.Id("hplogo"));
        img.Click();
        button.Click();
        WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        Assert.Throws(Is.TypeOf<NoSuchElementException>().Or.TypeOf<WebDriverTimeoutException>(),() => { 
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[@href='https://www.microsoft.org/']")));
            });
        }
    }
    [Test]
    public void EnterTextTest()
    {
        ChromeDriver driver = new ChromeDriver("/home/dima/DotNet/selenium/AutoTests");
        Google2 google2 = new Google2(driver);
        google2.GoToGoogle();
        google2.enterSearchText("Hello google!");
        Thread.Sleep(2000);
        google2.clickOk();
        driver.Close();
    }

    [Test]
    public void RemoteChromeTest()
    {
        ChromeOptions chrome = new ChromeOptions();
        // options.AddExtensions("/full/path/to/extension.crx");

        RemoteWebDriver driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), chrome);
        var google = new Google(driver);
        google.NavigateToGoogle();
        google.SearchText("selenium");
        ResultPage result = google.Click();
        Assert.True(result.isResultPresent());
        driver.Quit();
    }

    [Test]
    public void RemoteFirefoxTest()
    {
        FirefoxOptions firefox = new FirefoxOptions();

        RemoteWebDriver driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), firefox);
        var google = new Google(driver);
        google.NavigateToGoogle();
        google.SearchText("selenium");
        ResultPage result = google.Click();
        Assert.True(result.isResultPresent());
        driver.Quit();
    }
    }
}