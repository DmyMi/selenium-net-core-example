using OpenQA.Selenium;

namespace Pages
{
    public class Google2
    {
        private string url = "https://google.com.ua/";
        private IWebDriver driver;

        public IWebElement searchInput
        {
            get {return driver.FindElement(By.Name("q")); }
        }

        public IWebElement okButton
        {
            get {return driver.FindElement(By.Name("btnK"));}
        }

        public Google2(IWebDriver d)
        {
            this.driver = d;
        }

        public void GoToGoogle()
        {
            driver.Navigate().GoToUrl(url);
        }

        public void enterSearchText(string text)
        {
            searchInput.Clear();
            searchInput.SendKeys(text);
        }

        public void clickOk()
        {
            searchInput.SendKeys(Keys.Enter);
        }
    }
}