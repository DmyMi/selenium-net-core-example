﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Pages
{
    public class Google
    {
        private IWebDriver driver;

        private WebDriverWait wait;
        private readonly string url = @"https://google.com/";

        private IWebElement search
        {
            get {return this.driver.FindElement(By.CssSelector("#lst-ib"));}
        }

        private IWebElement btn
        {
            get { return this.driver.FindElement(By.CssSelector("input[type='submit']"));}
        }

        private IWebElement img
        {
            get {return this.driver.FindElement(By.CssSelector("#hplogo"));}
        }

        public Google(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        }

        public void NavigateToGoogle()
        {
            this.driver.Navigate().GoToUrl(this.url);
        }

        public void SearchText(string txt)
        {
            search.Clear();
            search.SendKeys(txt);
        }

        public ResultPage Click()
        {
            this.img.Click();
            this.btn.Click();
            return new ResultPage(this.driver);
        }
    }
}
