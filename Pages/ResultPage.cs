using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Pages
{
    public class ResultPage
    {
        private IWebDriver driver;
        private WebDriverWait wait;
        public ResultPage(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        }

        public Boolean isResultPresent()
        {
            try
            {
                var el = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[@href='https://www.seleniumhq.org/']")));
                if (el != null) {
                    return true;
                } else {
                    return false;
                }
            }
            catch(NoSuchElementException)
            {
                return false;
            }
        }
    }
}